<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntrianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antrian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tgl_periksa');
            $table->integer('urutan');
            $table->integer('pasien_id')->foreign('pasien_id')->references('id')->on('pasien')->onDelete('cascade');
            $table->integer('status_id')->foreign('status_id')->references('id')->on('status')->onDelete('cascade');
            $table->integer('poli_id')->foreign('poli_id')->references('id')->on('poli')->onDelete('cascade');
            $table->integer('jadwal_klinik_id')->foreign('jadwal_klinik_id')->references('id')->on('jadwal_klinik')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antrian');
    }
}
