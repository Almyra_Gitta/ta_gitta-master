<?php

namespace App\Models;

use App\Models\Role;
use App\Models\Pegawai;
use App\Models\Pasien;
use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'username', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function admin() {
        return $this->hasMany(Admin::class, 'user_id');
    }

    public function pegawai() {
        return $this->hasMany(Pegawai::class, 'user_id');
    }

    public function pasien() {
        return $this->hasMany(Pasien::class, 'user_id');
    }
}
