<?php

namespace App\Models;

use App\Models\Poli;
use App\Models\JadwalKlinik;
use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = "dokter";
    protected $fillable = [
        'sip', 'nama', 'alamat', 'jenis_kelamin', 'no_telp', 'poli_id', 'jadwal_klinik_id'
    ];

    public function poli() {
        return $this->belongsTo(Poli::class, 'poli_id', 'id');
    }

    public function jadwal_klinik() {
        return $this->belongsTo(JadwalKlinik::class, 'jadwal_klinik_id', 'id');
    }
}
