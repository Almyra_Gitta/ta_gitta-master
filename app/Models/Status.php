<?php

namespace App\Models;

use App\Models\Antrian;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";
    protected $fillable = [
        'status'
    ];
    public function antrian() {
        return $this->hasMany(Antrian::class, 'status_id');
    }
}
