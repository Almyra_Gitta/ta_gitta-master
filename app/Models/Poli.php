<?php

namespace App\Models;

use App\Models\Dokter;
use App\Models\Antrian;
use App\Models\JadwalKlinik;
use Illuminate\Database\Eloquent\Model;

class Poli extends Model
{
    protected $table = "poli";
    protected $fillable = [
        'nama', 'keterangan'
    ];

    public function dokter() {
        return $this->hasMany(Dokter::class, 'poli_id');
    }

    public function antrian() {
        return $this->hasMany(Antrian::class, 'poli_id');
    }

    public function jadwal_klinik() {
        return $this->hasMany(JadwalKlinik::class, 'poli_id');
    }
    
}
