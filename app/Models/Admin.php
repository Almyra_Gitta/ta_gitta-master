<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = "admin";
    protected $fillable = [
        'nama', 'user_id'
    ];
    public function user() {
        return $this->hasOne(User::class, 'user_id');
    }
}
