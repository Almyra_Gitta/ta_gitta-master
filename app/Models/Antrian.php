<?php

namespace App\Models;

use App\Models\Pasien;
use App\Models\Status;
use App\Models\Poli;
use App\Models\JadwalKlinik;
use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    protected $table = "antrian";
    protected $fillable = [
        'tgl_periksa', 'urutan', 'pasien_id', 'status_id', 'poli_id', 'jadwal_klinik_id'
    ];
    
    public function pasien() {
        return $this->belongsTo(Pasien::class, 'pasien_id', 'id');
    }

    public function status() {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    public function poli() {
        return $this->belongsTo(Poli::class, 'poli_id', 'id');
    }

    public function jadwal_klinik() {
        return $this->belongsTo(JadwalKlinik::class, 'jadwal_klinik_id', 'id');
    }
}
