<?php

namespace App\Models;

use App\Models\Poli;
use App\Models\Dokter;
use Illuminate\Database\Eloquent\Model;

class JadwalKlinik extends Model
{
    protected $table = "jadwal_klinik";
    
    protected $fillable = [
        'jam_buka', 'jam_tutup', 'poli_id',  'kuota'
    ];

    public function poli() {
        return $this->belongsTo(Poli::class, 'poli_id', 'id');
    }

    public function jadwal_klinik(){
        return $this->belongsTo(Dokter::class, 'jadwal_klinik_id', 'id');
    }
}
