<?php

namespace App\Models;

use App\Models\User;
use App\Models\Antrian;
use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = "pasien";
    protected $fillable = [
        'no_kartu', 'nik', 'nama', 'tempat_lahir', 'tgl_lahir', 'alamat', 'no_telp', 'jenis_kelamin', 'user_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function antrian() {
        return $this->hasMany(Antrian::class, 'pasien_id');
    }
}
