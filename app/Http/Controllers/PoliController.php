<?php

namespace App\Http\Controllers;

use App\Models\Poli;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class PoliController extends Controller
{
    public function store(Request $request){
        $nama = $request->input('nama');
        $keterangan = $request->input('keterangan');

        $save = Poli::insert([
            'nama' => $nama,
            'keterangan' => $keterangan
        ]);

        if($save){
            return response()->json([
              'status' => true,
              'message' => 'Berhasil menambahkan poli',
              'data' => $save
            ]);
          }else{
            return response()->json([
              'status' => false,
              'message' => 'Gagal menambahkan poli'
            ]);
          }
    }

    public function getAll(Request $request){
      $data = Poli::all();
      return response()->json([
        'status' => true,
        'message' => 'Data poli',
        'data' => $data
      ]);
    }

    //INI BACKEND//
    public function index() {
      $jumlahPoli = Poli::count();

      $params = [
          'jumlahPoli' => $jumlahPoli,
      ];

      // mengambil data dari table
      $poli = DB::table('poli')->get();

    // mengirim data ke view index
      return view('backend.poli.index', $params, ['poli' => $poli]);
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');

        if($id) {
            $data = Poli::find($id);
            //dd($data);
        } else
        {
            $data = new Poli();
        }
        $params =[
            'title' => 'Manajemen Tools Certification',
            'data' => $data,
        ];
        return view('backend.poli.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = Poli::find($id);
        } else
        {
            $data = new Poli();
            $checkData = Poli::where(['nama' => $request->no_buku])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Buku Lisensi sudah tersedia!</div>";
            }
        }

        $data->nama = $request->nama;
        $data->keterangan = $request->keterangan;
            
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Poli Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            // dd($ex);
            return "<div class='alert alert-danger'>Add Poli Failed! Poli not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

      $id = intval($request->input('id', 0));
      $data = Poli::find($id);

      try {
          $data->delete();
          return "
          <div class='alert alert-success'>Poli Remove Success!</div>
          <script> scrollToTop(); reload(1500); </script>";
      } catch(\Exception $ex){
          return "<div class='alert alert-danger'>Remove Failed! Poli not removed!</div>";
      }
  }
  //INI BACKEND//
}
