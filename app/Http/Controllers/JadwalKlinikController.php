<?php

namespace App\Http\Controllers;

use App\Models\JadwalKlinik;
use App\Models\Poli;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;

class JadwalKlinikController extends Controller
{
    public function getJadwal(Request $request, $id){
        $data = JadwalKlinik::where('poli_id', '=' , $id);
        return response()->json([
            'status' => true,
            'message' => 'Berhasil mengantri',
            'data' => $data
          ]);
    }

    //INI BACKEND//
    public function index() {
        $jumlahJadwalKlinik = JadwalKlinik::count();
  
        $params = [
            'jumlahJadwalKlinik' => $jumlahJadwalKlinik,
        ];
  
        // mengambil data dari table
        $jadwal_klinik = DB::table('jadwal_klinik')->get();
  
      // mengirim data ke view index
        return view('backend.jadwal_klinik.index', $params, ['jadwal_klinik' => $jadwal_klinik]);
      }
  
      public function addMaster(Request $request)
      {
          $id = $request->input('id');
          $dataPoli = Poli::all();
  
          if($id) {
              $data = JadwalKlinik::find($id);
              //dd($data);
          } else
          {
              $data = new JadwalKlinik();
          }
          $params =[
              'title' => 'Manajemen Tools Certification',
              'dataPoli' => $dataPoli,
              'data' => $data,
          ];
          return view('backend.jadwal_klinik.formMaster', $params);
      }
  
      public function saveMaster(Request $request)
      {
          $id = intval($request->input('id', 0));
  
          if($id) {
              $data = JadwalKlinik::find($id);
          } else
          {
              $data = new JadwalKlinik();
              $checkData = JadwalKlinik::where(['id' => $request->id])->first();
              if($checkData){
                  return "<div class='alert alert-danger'>Data Buku Lisensi sudah tersedia!</div>";
              }
          }
  
          $data->jam_buka = $request->jam_buka;
          $data->jam_tutup = $request->jam_tutup;
          $data->kuota = $request->kuota;
          $data->poli_id = $request->poli_id;
              
          try {
              $data->save();
              return "
              <div class='alert alert-success'> Add Jadwal Success!</div>
              <script> scrollToTop(); reload(1500); </script>";
          } catch (\Exception $ex){
              dd($ex);
              return "<div class='alert alert-danger'>Add Jadwal Failed! Jadwal not saved!</div>";
          }
      }
  
      public function deleteMaster(Request $request){
  
        $id = intval($request->input('id', 0));
        $data = JadwalKlinik::find($id);
  
        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Jadwal Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Jadwal not removed!</div>";
        }
    }
    //INI BACKEND//
}
