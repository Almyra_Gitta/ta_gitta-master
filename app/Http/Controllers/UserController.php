<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register(Request $request) {
      $user = User::create([
        'username' => $request->input('username'),
        'role_id'  => 3,
        'password' => Hash::make($request->input('password')),
      ]);

      if($user){
        return response()->json([
          'status' => true,
          'message' => 'Berhasil daftar',
          'data' => $user
        ]);
      }else{
        return response()->json([
          'status' => false,
          'message' => 'Gagal daftar'
        ]);
      }
    }

    public function login(Request $request) {
      $username = $request->input('username');
      $password = $request->input('password');
      $user = User::where('username', $username)->first();
      // echo $user;

      if($user){
        if(Hash::check($password, $user->password)){
          return response()->json([
            'status' => true,
            'message' => 'Berhasil login',
            'data' => $user
          ]);
        }else{
          return response()->json([
            'status' => false,
            'message' => 'Password salah'
          ]);
        }
      }else{
        return response()->json([
          'status' => false,
          'message' => 'Username belum terdaftar'
        ]);
      }
    }

    public function getPasien(Request $request){
      $user = User::where('role_id', '=', 3)->get();
      return response()->json([
        'status' => true,
        'message' => 'Data ditemukan',
        'data' => $user
      ]);
    }
}
