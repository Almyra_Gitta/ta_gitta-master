<?php

namespace App\Http\Controllers;

use App\Models\JadwalKlinik;
use App\Models\Poli;
use App\Models\Dokter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;

class DokterController extends Controller
{
    //INI BACKEND//
    public function index() {
        $jumlahDokter = Dokter::count();
  
        $params = [
            'jumlahDokter' => $jumlahDokter,
        ];
  
        // mengambil data dari table
        $dokter = DB::table('dokter')->get();
  
      // mengirim data ke view index
        return view('backend.dokter.index', $params, ['dokter' => $dokter]);
      }
  
      public function addMaster(Request $request)
      {
          $id = $request->input('id');
          $dataJadwalKlinik = JadwalKlinik::all();
          $dataPoli = Poli::all();
  
          if($id) {
              $data = Dokter::find($id);
              //dd($data);
          } else
          {
              $data = new Dokter();
          }
          $params =[
              'title' => 'Manajemen Tools Certification',
              'dataJadwalKlinik' => $dataJadwalKlinik,
              'dataPoli' => $dataPoli,
              'data' => $data,
          ];
          return view('backend.dokter.formMaster', $params);
      }
  
      public function saveMaster(Request $request)
      {
          $id = intval($request->input('id', 0));
  
          if($id) {
              $data = Dokter::find($id);
          } else
          {
              $data = new Dokter();
              $checkData = Dokter::where(['id' => $request->id])->first();
              if($checkData){
                  return "<div class='alert alert-danger'>Data Buku Lisensi sudah tersedia!</div>";
              }
          }
  
          $data->sip = $request->sip;
          $data->nama = $request->nama;
          $data->alamat = $request->alamat;
          $data->jenis_kelamin = $request->jenis_kelamin;
          $data->no_telp = $request->no_telp;
          $data->poli_id = $request->poli_id;
          $data->jadwal_klinik_id = $request->jadwal_klinik_id;
              
          try {
              $data->save();
              return "
              <div class='alert alert-success'> Add Dokter Success!</div>
              <script> scrollToTop(); reload(1500); </script>";
          } catch (\Exception $ex){
              // dd($ex);
              return "<div class='alert alert-danger'>Add Dokter Failed! Dokter not saved!</div>";
          }
      }
  
      public function deleteMaster(Request $request){
  
        $id = intval($request->input('id', 0));
        $data = Dokter::find($id);
  
        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Dokter Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Dokter not removed!</div>";
        }
    }
    //INI BACKEND//
}
