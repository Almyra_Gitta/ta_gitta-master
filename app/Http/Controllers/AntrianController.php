<?php

namespace App\Http\Controllers;

use App\Models\Antrian;
use App\Models\JadwalKlinik;
use Illuminate\Http\Request;

class AntrianController extends Controller
{
    public function store(Request $request){
        $poli = $request->input('poli_id');
        $jadwal = $request->input('jadwal_klinik_id');
        $kuota = JadwalKlinik::where('id','=', $jadwal)->get();
        $total = Antrian::whereDate('tgl_periksa', '=', date("Y-m-d"))->where('poli_id', '=', $poli)->where('jadwal_klinik_id', '=', $jadwal)->count();					
        if($kuota->kuota < $total){
            $save = Antrian::insert([
                'tgl_periksa' => $request->input('tgl_periksa'),
                'urutan' => $total + 1,
                'pasien_id' => $request->input('pasien_id'),
                'status_id' => $request->input('status_id'),
                'poli_id' => $poli,
                'jadwal_klinik_id' => $jadwal
            ]);
            if($save){
                return response()->json([
                    'status' => true,
                    'message' => 'Berhasil mengantri',
                    'data' => $save
                  ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal mengantri'
                  ]); 
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Antrian penuh. Ambil jadwal lain'
              ]);
        }
    }

    public function getMyAntrian(Request $request){
        $date = $request->input('tgl_periksa');
        $user = $request->input('pasien_id');
        $poli = $request->input('poli_id');

        $total = Antrian::whereDate('tgl_periksa', '=', $date)->where('poli_id', '=', $poli)->count();
        $urutan = Antrian::whereDate('tgl_periksa','=', $date)->where('pasien_id', '=', $user)->get();
        $urutanNow = Antrian::whereDate('tgl_periksa','=', $date)->where('status_id', '=', 2)->orderBy('id','DESC')->first();
        
        return response()->json([
            'status' => true,
            'message' => 'Data Antrian',
            'data' => [
                'total' => $total,
                'antrianku' => $urutan[0]->urutan,
                'antrianNow' => $urutanNow->urutan
            ]
          ]);
    }
}
