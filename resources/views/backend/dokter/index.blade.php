@extends('layouts.app')

@section('content')
<title>Daftar Dokter &mdash; Rumah Sakit Semen Gresik</title>

<section class="section">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Dokter</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlahDokter }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Dokter</h4>
                    </div>
                    <!-- <p class="section-lead">
                        <a href="{{ url('poli/create') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p> -->
                    <div class="section-lead">
                        <button onclick="loadModal(this)" title="" target="/dokter/addMaster" type="button" class="btn btn-icon icon-left btn-primary">
                            <i class="fas fa-edit">Tambah Data</i>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-dokter">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Shift Dokter</th>
                                        <th>Nama Dokter</th>
                                        <th>Alamat Dokter</th>
                                        <th>Jenis Kelamin</th>
                                        <th>No. Telp Dokter</th>
                                        <th>Poli</th>
                                        <th>Jadwal Klinik</th>
                                        <th>Create At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dokter as $p => $item)
                                    <tr>
                                        <td>
                                            {{ $p+1 }}
                                        </td>
                                        <td>{{ $item->sip }}</td>
                                        <td>
                                            {{ $item->nama }}
                                        </td>
                                        <td>
                                            {{ $item->alamat }}
                                        </td>
                                        <td>
                                            {{ $item->jenis_kelamin }}
                                        </td>
                                        <td>
                                            {{ $item->no_telp }}
                                        </td>
                                        <td>
                                            Poli with ID {{ $item->poli_id }}
                                        </td>
                                        <td>
                                            Klinik with ID {{ $item->jadwal_klinik_id }}
                                        </td>
                                        <td>
                                            {{ $item->created_at }}
                                        </td>
                                
                                        <td>
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button onclick="loadModal(this)" title="" target="/dokter/addMaster" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/dokter/deleteMaster", data, "#modal-output");
            })
        }

        function editMaster(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/dailyreportk3/addMaster", data, '#modal-output');
        }

        var table = $('#table-dokter');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
@endsection