<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="{{asset('public/assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Dokter</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">RS SEMEN GRESIK</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:green; background-color:green">
                <strong style="background-color:green; margin-left:5px; color:white">Keterangan Dokter</strong>
            </div>
            <div class="row go_form">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Shift Dokter :</label>
                                <input type="text" class="form-control daterange-cus" value="{{ $data->sip}}" name="sip">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nama Dokter :</label>
                                <input type="text" class="form-control daterange-cus" value="{{ $data->nama}}" name="nama">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Jenis Kelamin :</label>
                                <select class="form-control" name="jenis_kelamin">
                                    <option <?php if($data->jenis_kelamin =='Laki-Laki') {echo "selected";}?> value="Laki-Laki">Laki-Laki</option>
                                    <option <?php if($data->jenis_kelamin =='Wanita') {echo "selected";}?> value="Wanita">Wanita</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Alamat Dokter :</label>
                                <input type="text" class="form-control daterange-cus" value="{{ $data->alamat}}" name="alamat">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>No. Telepon :</label>
                                <input type="text" class="form-control daterange-cus" value="{{ $data->no_telp}}" name="no_telp">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:green">
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Poli :</label>
                                    <select name="poli_id" class="form-control">
                                    @foreach($dataPoli as $item)
                                        <option value="{{ $item->id }}" @if(!is_null($data)) @if($item->id == $data->poli_id) selected="selected" @endif @endif>{{ $item->id}} - {{ $item->nama}}</option>
                                
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jadwal Klinik :</label>
                                    <select name="jadwal_klinik_id" class="form-control">
                                    @foreach($dataJadwalKlinik as $item)
                                        <option value="{{ $item->id }}" @if(!is_null($data)) @if($item->id == $data->jadwal_klinik_id) selected="selected" @endif @endif>{{ $item->jam_buka}} - {{ $item->jam_tutup}}</option>
                                
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dokter/saveMaster', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
