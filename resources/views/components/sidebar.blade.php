<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}"><img src="{{asset('public/assets/img/shelogo.png')}}" class="img-responsive" alt="logo" width="40"> RS SEMEN GRESIK</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <!-- <a href="{{ route('home') }}">SHE</a> -->
            <a href="{{ route('home') }}"><img src="{{asset('public/assets/img/shelogo.png')}}" class="img-responsive" alt="logo" width="40"></a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-th-large"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">Master Data</li>
            <li class="dropdown">
                
                <li class="{{ request()->is('antrian') ? 'active' : '' }}">
                    <a href="{{ url('/antrian') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data Antrian</span></a>
                </li>
                <li class="{{ request()->is('dokter') ? 'active' : '' }}">
                    <a href="{{ url('/dokter') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data Dokter</span></a>
                </li>
                <li class="{{ request()->is('jadwal_klinik') ? 'active' : '' }}">
                    <a href="{{ url('/jadwal_klinik') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data Jadwal Klinik</span></a>
                </li>
                <!-- <li class="{{ request()->is('pasien') ? 'active' : '' }}">
                    <a href="{{ url('/pasien') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data Pasien</span></a>
                </li> -->
                <li class="{{ request()->is('poli') ? 'active' : '' }}">
                    <a href="{{ url('/poli') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data Poli</span></a>
                </li>
                <!-- <li class="{{ request()->is('users') ? 'active' : '' }}">
                    <a href="{{ url('/users') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data User</span></a>
                </li> -->
            </li>
        </ul>
    </aside>
</div>
