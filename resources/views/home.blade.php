@extends('layouts.app')

@section('content')
<title>RS SEMEN GRESIK Dashboard | RSSG</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/shelogo.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center>
                      <h5>RS SEMEN GRESIK Dashboard</h5>
                    </center>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
