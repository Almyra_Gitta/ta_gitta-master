<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// pasien
Route::get('/pasien/all', 'UserController@getPasien');
Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');

// poli
Route::get('/poli', 'PoliController@getAll');

// antrian
Route::post('/antrian', 'AntrianController@store');
Route::post('/antrian/my', 'AntrianController@getMyAntrian');

// jadwal
Route::get('/jadwal/poli/{id}', 'JadwalKlinikController@getJadwal');
