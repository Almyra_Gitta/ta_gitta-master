<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'poli'], function () {
    Route::get('/', 'PoliController@index');
    Route::post ( '/addMaster', 'PoliController@addMaster' );
    Route::post ( '/saveMaster', 'PoliController@saveMaster' );
    Route::post('/deleteMaster', 'PoliController@deleteMaster');
});

Route::group(['prefix' => 'jadwal_klinik'], function () {
    Route::get('/', 'JadwalKlinikController@index');
    Route::post ( '/addMaster', 'JadwalKlinikController@addMaster' );
    Route::post ( '/saveMaster', 'JadwalKlinikController@saveMaster' );
    Route::post('/deleteMaster', 'JadwalKlinikController@deleteMaster');
});

Route::group(['prefix' => 'dokter'], function () {
    Route::get('/', 'DokterController@index');
    Route::post ( '/addMaster', 'DokterController@addMaster' );
    Route::post ( '/saveMaster', 'DokterController@saveMaster' );
    Route::post('/deleteMaster', 'DokterController@deleteMaster');
});